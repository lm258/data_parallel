__kernel void matmul(
   __global float* in_a,
   __global float* in_b,
   __global float* out,
   const unsigned int count)
{
    int i = get_global_id(0);
    int j = get_global_id(1);
    int q = 0;
    out[(i * count) + j ] = 0;
    for(q=0; q < count; q++){
        out[ (i*count) + j ] += in_a[ (i*count) + q ] * in_b[ (q*count) + j];
    }
}