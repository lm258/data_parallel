__kernel void square(
   __global float* input,
   __global float* output,
   const unsigned int count,
   const unsigned int ssize)
{
    int i = get_global_id(0);
    int j;
    for(j=i; j<count; j+=ssize ){
        output[j] = input[j] * input[j];
    }
}