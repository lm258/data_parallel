__kernel void square(
   __global float* input,
   __global float* output,
   const unsigned int count,
   const unsigned int ssize)
{
  int i = get_global_id(0);
  int p = i*ssize;
  int j;
  for(j=p; j<(p+ssize); j++ )
    output[j] = input[j] * input[j];
}
