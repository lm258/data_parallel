-- Function counts elements in string
count         :: [a] -> Integer
count []      = 0
count (h:tl)  = 1 + count(tl)

-- Function for multilication
mult     :: Integer -> Integer -> Integer
mult x y = x * y

--define pi
pi_def = 3.14

--calulate area of circle
--circle_area   :: Integer -> ( (Integer) -> Integer)
circle_area r = pi_def * (r^2)

--concatanate strings
concat_str     :: String -> String -> String
concat_str x y = x ++ y

--Sum numbers in list
sumTill   :: Integer -> Integer
sumTill 0 = 0
sumTill n = n + sumTill (n - 1)

--Fibonacci numbers
fib   :: Integer -> Integer
fib 0 = 1
fib 1 = 1
fib n = fib (n-1) + fib (n-2)

--Product of array elements
prod        :: [Integer] -> Integer
prod []     = 0
prod [x]    = x
prod (h:tl) = h * prod tl

--Returns true if number is positive
positive   :: Integer -> Bool
positive x = if x < 0 then False else True 

--Returns true if x is in list [....]
contains          :: Integer -> [Integer] -> Bool
contains x []     = False
contains x (h:tl) = if (x == h) then True else contains x tl

--This function returns the intersecting elements of two arrays
intersects          :: [Integer] -> [Integer] -> [Integer]
intersects [] y     = []
intersects (h:tl) y = if (contains h y) then [h] ++ intersects tl y else intersects tl y

--Return powers up to n
powers   :: Integer -> [Integer]
powers 0 = [0]
powers x = [x*x] ++ powers (x - 1)

--Define our tree data structure
data Tree a  = Leaf a 
             | Branch (Tree a) (Tree a)
             deriving (Show,Read,Eq)

--Count the length of longest branch
depth              :: (Tree a) -> Integer
depth (Leaf a)     = 0
depth (Branch x y) = if (depth x > depth y) then 1 + depth x else 1 + depth y

--This function checks wether the tree is balanced or not
balanced              :: (Tree a) -> Bool
balanced (Leaf a)     = False
balanced (Branch x y) = if ( depth x == depth y ) then True else False

--This function will split a list
split    :: [a] -> ([a],[a])
split [] = ([], [])
split x  = splitAt (((length x) + 1) `div` 2) x

--This function will split an array in two
mkTree     :: [Integer] -> (Tree Integer)
mkTree [x] = (Leaf x)
mkTree lst = Branch (mkTree flf) (mkTree slf)
             where splt = split lst
                   flf  = fst splt
                   slf  = snd splt

--Flatten Tree
flatten              :: (Tree Integer) -> [Integer]
flatten (Leaf x)     = [x]
flatten (Branch x y) = flatten x ++ flatten y

--Simple HCF function
hcf     :: Integer -> Integer -> Integer
hcf x 0 = x
hcf x y = hcf y (rem x y)

--Are two numbers relativley prime?
relprime     :: Integer -> Integer -> Bool
relprime x y = hcf x y == 1

--This function calculates the eulers totient for n
euler x = length (filter (relprime x) [1..x] )

--Calcularte eulers som for each number in the range
euler_sum s e = sum ( map euler [s..e] )

main = putStrLn "Loaded Lab Excersises."