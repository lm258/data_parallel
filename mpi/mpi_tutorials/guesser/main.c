/*
   guess.c - Distributed number guessing game with MPI

   compile: mpicc -Wall -O -o guess guess.c
   run:     mpirun -np num_procs guess
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <mpi.h>

#define MAX_GUESS 100000000

/*Define a custom message type to store id*/
struct Message {
  int id;
  int value;
};
typedef struct Message Message;

/*Thinker logic*/
void thinker( players )
{
  int number;
  char reply;
  MPI_Status status;

  srand((unsigned int)time(NULL));

  reply = 'x';
  number = rand() % MAX_GUESS + 1;
  printf("0: (I'm thinking of %d)\n",number);
  Message theirGuess;

  while (players) {
    MPI_Recv(&theirGuess, sizeof(Message), MPI_CHAR, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
    if (theirGuess.value == number){
      reply = 'c';
      players--;
      printf("Player: %d has found the answer!\n", theirGuess.id);
    }
    else if (theirGuess.value > number)
      reply = 'h';
    else
      reply = 'l';
    printf("0: %d guessed %2d; I'm responding %c\n", theirGuess.id, theirGuess.value, reply);
    MPI_Send(&reply, 1, MPI_CHAR, theirGuess.id, 0, MPI_COMM_WORLD);
  }
}

/*Guesser logic*/
void guesser(int id)
{
  int high, low;
  char reply;
  MPI_Status status;

  sleep(1); srand((unsigned int)time(NULL));

  low = 1;
  high = MAX_GUESS;
  Message guess;
  guess.id = id;
  guess.value = rand() % MAX_GUESS + 1;
  printf("%d: I'm guessing %2d\n", id, guess.value);
  while (1) {
    MPI_Send(&guess, sizeof(Message), MPI_CHAR, 0, 0, MPI_COMM_WORLD);
    MPI_Recv(&reply, 1, MPI_CHAR, 0, 0, MPI_COMM_WORLD, &status);
    switch (reply) {
      case 'c': printf("%d: 0 replied %c\n", id, reply); return;
      case 'h': high = guess.value; break;
      case 'l': low = guess.value; break;
    }
    guess.value = (high + low) / 2;
    printf("%d: 0 replied %c; I'm guessing %2d\n", id, reply, guess.value);
  }
}


int main(int argc, char ** argv)
{
  int p, id;

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &p);
  MPI_Comm_rank(MPI_COMM_WORLD, &id);

  if (id == 0){
    printf("Number of players: %d\n", p);
    thinker(p-1);
  }
  else {
    guesser(id);
  }

  MPI_Finalize();
  return 0;
}