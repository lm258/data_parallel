/* 
   matrix4.c - Naive Parallel Matrix Multiplication
               using transpose & dot product 

   compile: mpicc -Wall -O -o matrix4 matrix4.c
   run:     mpirun -np num_procs matrix4 file
*/

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <time.h>

/* Aux fcts ------------------------------------------------------- */
int * allocVector(int n)
{
  return (int *)malloc(n*sizeof(int));
}

int ** allocMatrix(int m, int n)
{
  int ** newM = (int **)malloc(m*sizeof(int *));
  int i;
  for (i = 0; i < m; i++)
    newM[i] = allocVector(n);
  return newM;
}

void readMatrix(FILE * fin, int ** M, int m, int n)
{
  int i, j;
  for (i = 0; i < m; i++)
    for (j = 0; j < n; j++)
      fscanf(fin, "%d", &(M[i][j]));
}

void writeMatrix(FILE * fout, int ** M, int m, int n)
{
  int i, j;
  for (i = 0; i < m; i++) {
    for (j = 0; j < n; j++)
      fprintf(fout, "%d ", M[i][j]);
    putc('\n', fout);
  }
}


/* Aux fct: transpose m*n matrix into n*m matrix */
int ** transpose(int ** M, int m, int n)
{
  int ** MT;
  int i, j;
  MT = allocMatrix(n, m);
  for (i = 0; i < m; i++)
    for (j = 0; j < n; j++)
      MT[j][i] = M[i][j];
  return MT;
}


/* Aux fct: dot product of two vectors of length n */
int dotProd(int * V1, int * V2, int n)
{
  int dp = 0;
  int i;
  for (i = 0; i < n; i++)
    dp = dp + V1[i] * V2[i];
  return dp;
}

/* Precondition: workers >= 1 */
int ** matrixProdMaster(int ** M1, int ** M2T, int m, int n, int workers)
{
  int i, j;
  MPI_Status status;
  int ** M3 = allocMatrix(m, m);
  int s = m/workers; /* lower bound on #rows per worker */
  int r = m%workers; /* remaining rows */

  /* First r workers get sent s+1 rows of M1 each */
  for (i = 0; i < r; i++)
    for (j = 0; j < s+1; j++)
      MPI_Send( M1[i * (s+1) + j], n, MPI_INT, i+1, 0, MPI_COMM_WORLD);

  /* Remaining workers get sent s rows of M1 each */
  for (i = r; i < workers; i++)
    for (j = 0; j < s; j++)
      MPI_Send( M1[r + i * s + j], n, MPI_INT, i+1, 0, MPI_COMM_WORLD);

  /* All columns of M2 sent to all workers */
  for (j = 0; j < m; j++)
    for (i = 0; i < workers; i++)
    MPI_Send(M2T[j], n, MPI_INT, i+1, 0, MPI_COMM_WORLD);

  /* First r workers deliver s+1 rows of M3 each */
  for (i = 0; i < r; i++)
    for (j = 0; j < s+1; j++)
      MPI_Recv(M3[i * (s+1) + j], m, MPI_INT, i+1, 0, MPI_COMM_WORLD, &status);

  /* Remaining workers deliver s rows of M3 each */
  for (i = r; i < workers; i++)
    for (j = 0; j < s; j++)
    MPI_Recv(M3[r + i * s + j], m, MPI_INT, i+1, 0, MPI_COMM_WORLD, &status);
    return M3;
}

/* Worker: Compute 1 row of resulting m*n matrix, PC is the number of procs, id is our id */
void matrixProdWorker(int m, int n, int id, int pc)
{
  MPI_Status status;
  int dp, i,j;
  int s = m/pc; if (id <= m%pc) s++; /*Add the extra row to proccess*/
  int ** R = allocMatrix(s,n); /*Row as an array (receive at once)*/
  int * C = allocVector(n);
  int ** RR = allocMatrix(s,m); /*Result row for storing the result*/

  /**
   * Receive the R at once
   * This is our block for the processor to process
   * eg it could be 3 rows so we need to receive each
  */
  for(i=0; i < s; i++ ){
    MPI_Recv(R[i], n, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
  }

  for (i = 0; i < m; i++) {
    /*Receive in outer loop as we can reuse this*/
    MPI_Recv(C, n, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);

    /*Process everything in the inner loop*/
    for(j=0; j< s; j++ )
      R[j][i] = dotProd(R[j], C, n);
  }

  /*Now we should send zee full block back*/
  for(i=0;i<s;i++)
    MPI_Send(&RR[i], m, MPI_INT, 0, 0, MPI_COMM_WORLD);
}

/* Main fct ------------------------------------------------------- */

int main(int argc, char ** argv)
{
  int p, id, m, n;
  FILE * fin;

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &p);
  MPI_Comm_rank(MPI_COMM_WORLD, &id);

  if (id == 0) {

    /*Open the file and read matrix into M1 and M2*/
    fin = fopen(argv[1], "r");
    fscanf(fin, "%d %d", &m, &n);
    int ** M1 = allocMatrix(m,n);
    int ** M2 = allocMatrix(n,m);
    readMatrix(fin, M1, m, n);
    readMatrix(fin, M2, n, m);
    fclose(fin);

    int i;
    /*Send the array size to each processor*/
    for(i = 1; i < p; i++) {
      MPI_Send(&m, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
      MPI_Send(&n, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
    }

    /*This is a limitation we need to look at*/
    int ** M2T = transpose(M2, n, m);
    int ** M3 = matrixProdMaster(M1, M2T, m, n, p-1); /*Second argument is one we are working with*/
    writeMatrix(stdout, M3, m, m);
    printf("I am done\n");
  } else {
    MPI_Status status;

    MPI_Recv(&m, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
    MPI_Recv(&n, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);

    /**
     * Need to split the work up here per SLAVE that will work
     * should look into the variable 'n'
    */
    matrixProdWorker(m, n, id, p-1);
  }

  MPI_Finalize();
  return 0;
}