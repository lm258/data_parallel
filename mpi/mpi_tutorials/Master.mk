SHELL = /bin/bash

all: source
	mpicc -Wall -O $(PROJECT) -o $(OUT)

run:
	mpiexec -n $(PROC) `pwd`/$(OUT) $(DATA)

run-cluster:
	mpiexec -n $(PROC) -f $(HOST) `pwd`/$(OUT) $(DATA)

source:
	@ if ! [ hash mpicc 2>/dev/null ]; then \
		echo "Loading bashrc...."; \
		source ~/.bashrc; \
	fi