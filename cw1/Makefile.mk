#-g -pg are for gprof
#gnuplot for parallel
all: build

build:
	gcc -Wall $(C_SOURCES) -o $(C_OUT) $(C_INCLUDE) $(C_LIBRARIES) $(FLAGS)

profile:
	@gcc -Wall $(C_SOURCES) -o $(C_OUT)_prof -g -pg $(FLAGS)
	@./$(C_OUT)_prof $(PROFILE_ARGS)
	@gprof -Q $(C_OUT)_prof > gprof.txt
	@rm $(C_OUT)_prof
	@rm gmon.out

cachegrind: build
	valgrind --tool=cachegrind ./$(C_OUT) $(PROFILE_ARGS) 2> cachegrind.txt
	@rm cachegrind.out.*

clean:
	@rm -f $(C_OUT)
	@rm -f gprof_out.txt