#!/bin/bash

# Echo the main graph file plot in the plot file
echo -n "set samples 800, 800
      set title \"$1\"
      set xlabel \"cores\"
      set ylabel \"time seconds\"
      set title  font \",20\" norotate
      set datafile separator \",\"
      set term png
      set output '$2.png'
      set style data points
      set xrange [8:256]
      plot " > plot.gnu

# Echo all the plot data in the current directory into our graph file
for d in `ls *.dat`; do
    title=${d%.dat}
    echo "\"$d\" using 1:2 title '$title' with linespoints,\\" >> plot.gnu
done
sed -i '$s/..$//' plot.gnu

if [ -f with_prime_check_iterations.txt ]; then
  sort -k1 -n with_prime_check_iterations.txt > file1.txt
  sort -k1 -n no_prime_check_iterations.txt > file2.txt
  echo `while read line1 && read -u 3 line2; do d=$((${line1#*' '}-${line2#*' '}));echo $d; done < file1.txt 3< file2.txt` > iterations.diff
  rm *.txt
fi

#Create the PNG from the plot data
gnuplot plot.gnu
rm -rf plot.gnu
