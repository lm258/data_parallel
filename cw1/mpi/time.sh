#!/bin/bash
#Arguments are $1=Program to time
#              $2=Make command ie run or run-cluster
for i in `seq $3 1000 $4`; do
    times=(0 0 0)
    for j in 0 1 2; do
        times[$j]=`make -s RANGE="1 $i" P_COUNT="$5" $2`;
    done
    utime=`echo "( ${times[0]}+${times[1]}+${times[2]} )/3" | bc -l`
    echo "$i, $utime";
    echo "$i, $utime" >> $1.dat;
done