#!/bin/bash
total=0
for i in `cat hosts`; do 
    val=`ssh $i "top -bn1 | grep 'Cpu(s)' |  sed 's/.*, *\([0-9.]*\)%* id.*/\1/' |  awk '{print \\$2}' | cut -d'%' -f 1"`
    echo $val
done
