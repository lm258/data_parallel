#!/bin/bash

#Check the sequential.dat file is present
if [ ! -f sequential.dat ]; then
    echo "Sequential.dat file needs to be present";
    exit;
fi;

#Loop over all files except sequential.dat
for file in `ls *.dat | grep -P '^(?!sequential)(.*\.dat)'`; do
    cline=1
    while read line; do
        #Get our file name no extension
        fname=${file%.dat}

        #Get our lines check they are valid
        seq_lne=`cat sequential.dat | head -$cline | tail -1`
        seq_tme=`echo $seq_lne | awk '{print $2}'`
        seq_rng=`echo $seq_lne | awk '{print $1}'`
        fil_lne=`cat $file | head -$cline | tail -1`
        fil_rng=`echo "$fil_lne"| awk '{ print $1 }'`
        fil_tme=`echo "$fil_lne"| awk '{ print $2 }'`

        #Make sure the lines have the same increment
        if [ ! "$seq_rng"="$fil_rng" ];
        then
            echo "Ranges between files are not equal[ '$seq_rng' != '$fil_rng' ]"
            exit
        fi

        #Calculate the speedup
        speedup=`echo "$seq_tme/$fil_tme" | bc -l`

        #Echo speedup with the increment back into the file
        echo "$seq_rng $speedup" >> $fname.sudat
        cline=$(($cline+1))
    done < $file
done

# Echo the main graph file plot in the plot file
echo -n "set samples 800, 800
      set title \"$1\"
      set xlabel \"range 1 - x\"
      set ylabel \"speed up\"
      set title  font \",20\" norotate
      set datafile separator \",\"
      set term png
      set output 'speedup.png'
      set style data points
      plot " > plot.gnu

# Echo all the plot data in the current directory into our graph file
for d in `ls *.sudat`; do
    title=${d%.sudat}
    echo "\"$d\" using 1:2 title '$title' with linespoints,\\" >> plot.gnu
done
sed -i '$s/..$//' plot.gnu

#Create the PNG from the plot data
gnuplot plot.gnu
rm -rf plot.gnu
rm -rf `ls *.dat | grep -P '^(?!sequential)(.*\.dat)'`
rm -rf `ls *.sudat`