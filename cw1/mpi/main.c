/**
 * (c) Lewis Maitland 2015
 * MPI C Eulers Totient implementation
 * for calculating totients in parallel
 * lm258@hw.ac.uk
 *    MACS
 *    Heriot-Watt University
 *    Edinburgh
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <inttypes.h>
#include <time.h>
#include <mpi.h>
#include <math.h>
#include "euler.h"

/*Only used for when we want to track the number of iterations*/
long iterations = 0;

/*Define a custom message type to store id, range and sum*/
struct Message {
  uint16_t id;
  uint32_t sum;
  uint32_t start;
  uint32_t end;
};
typedef struct Message Message;

/**
 * This function is used to create a message
*/
Message create_message( uint16_t id, uint32_t sum, uint32_t start, uint32_t end ){
    Message msg;
    msg.id    = id;
    msg.sum   = sum;
    msg.start = start;
    msg.end   = end;
    return msg;
}

/** 
 * This function calcul;ates the ulers sum
*/
long eulers_sum( long start, long end ){
    long i,sum;
    i = sum = 0;
    for(i=start; i<= end; i++){
        sum += euler(i);
    }
    return sum;
}

/**
 * This function is used for calculating the
 * end of the ith work nodes segment
*/
long worker_end( uint16_t i, uint16_t slaves, long length ) {
     return (((double)length * sin( (float)(i+1)/(float)(slaves+1) ) ))/sin(1);
}

/**
 * This part of the of the program will be used
 * for controlling and summing the results of the
 * workers.
*/
void controller( uint32_t start, uint32_t end, uint16_t slaves ) {
    uint32_t sum, work_s, work_e, length;
    uint16_t i;
    Message message;
    MPI_Status status;

    sum    = 0;
    length = end-start;
    work_s = worker_end(0, slaves, length);

    /*Send out the work load*/
    for( i=1; i <= slaves; i++ ) {
        work_e = worker_end(i, slaves, length)+1;

        message = create_message( i, 0, work_s, work_e );
        MPI_Send( &message, sizeof(Message), MPI_CHAR, i, 0, MPI_COMM_WORLD );
        work_s = work_e+1;
    }

    /*After sending the work calculate the controllers share ;)*/
    if( slaves == 0 ) {
        sum += eulers_sum( start, end );
    } else {
        sum += eulers_sum( start, worker_end(0, slaves, length)-1 );
    }

    /*Receive the sums back from workers*/
    for( i=0; i<slaves; i++ ){
        MPI_Recv( &message, sizeof(Message), MPI_CHAR, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
        sum += message.sum;
    }
    #if !defined(COUNT_ITERATIONS) && !defined(TIME_ONLY)
        printf("Total: %"PRIu32"\n" , sum);
    #endif
}

/**
 * Workers will be used for summing parts
 * of the totient range.
*/
void worker( uint16_t id ) {
    Message msg;
    MPI_Status status;
    double time_s, time_e;

    MPI_Recv(&msg, sizeof(Message), MPI_CHAR, 0, 0, MPI_COMM_WORLD, &status );
    time_s  = MPI_Wtime();
    msg.sum = eulers_sum(msg.start, msg.end);
    time_e  = MPI_Wtime();

    #ifdef NODE_TIMES
        printf("node[%d] \t\t time: %lf \t\t range: %"PRIu32"\n",id, time_e-time_s, msg.end-msg.start);
    #endif

    #ifdef COUNT_ITERATIONS
        printf("%d %ld\n", id, iterations );
    #endif
    MPI_Send( &msg, sizeof(Message), MPI_CHAR, 0, 0, MPI_COMM_WORLD );
}

int main(int argc, char ** argv) {
    int p, id;
    uint32_t start, end;
    double start_t, end_t;
    start = end = 0;

    /*Make sure that the range has been supplied*/
    if( argc != 3 ){
        fprintf( stderr, "Incorrect number of arguments supplied" );
        MPI_Abort( MPI_COMM_WORLD, 1 );
    }

    /*Init our MPI code*/
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &p);
    MPI_Comm_rank(MPI_COMM_WORLD, &id);

    /*Read in our range*/
    sscanf( argv[1], "%"PRIu32, &start);
    sscanf( argv[2], "%"PRIu32, &end);

    /*Get the starting time*/
    MPI_Barrier(MPI_COMM_WORLD);
    start_t  = MPI_Wtime();

    if(id == 0) {
        controller(start, end, p-1);
    } else {
        worker(id);
    }

    /*Get the ending time time*/
    MPI_Barrier(MPI_COMM_WORLD);
    end_t  = MPI_Wtime();

    #ifdef TIME_ONLY
    if( id == 0 ){
        printf("%02lf\n", end_t - start_t);
    }
    #endif

    MPI_Finalize();
  return 0;
}