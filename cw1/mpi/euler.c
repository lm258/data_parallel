#include "euler.h"

/*Highest common factor*/
long hcf(long x, long y)
{
  long t;

  while (y != 0) {
    t = x % y;
    x = y;
    y = t;
  } 
  return x;
}

int relprime(long x, long y) {
  return hcf(x, y) == 1;
}

int is_prime(long n){
  long i;
  if( n<= 1) return 0;
  if( n==2 ) return 1;
  if( n % 2 == 0 ) return 0;
  for(i=2; i*i <= n; i++){
    if( n%i == 0 ) return 0;
  }
  return 1;
}


// euler n = length (filter (relprime n) [1 .. n-1])

long euler(long n) {
  long length, i;
  length = 0;

  if(is_prime(n)==1){
    return n-1;
  }

  for (i = 1; i <=n; i++){
    if (relprime(n, i))
      length++;
  }

  return length;
}