#pragma once

extern long p_iterations;
extern long iterations;

long hcf(long x, long y);

int totient(int n);

int relprime(long x, long y);

long euler(long n);