# set terminal png transparent nocrop enhanced size 450,320 font "arial,8" 
# set output 'simple.8.png'
#set key bmargin left horizontal Right noreverse enhanced autotitle box lt black linewidth 1.000 dashtype solid
set samples 800, 800
set title "Sequential Execution Time" 
set title  font ",20" norotate
set datafile separator ","
set term png
set output 'out.png'
set style data points

plot "time.dat" using 1:2 with linespoints