# set terminal png transparent nocrop enhanced size 450,320 font "arial,8" 
# set output 'simple.8.png'
#set key bmargin left horizontal Right noreverse enhanced autotitle box lt black linewidth 1.000 dashtype solid
set samples 800, 800
set title "Execution Time of Sequential and Parallel on a Geforce GT 520" 
set title  font ",20" norotate
set datafile separator ","
set term png
set output 'comp.png'
set style data points
set xlabel "Upper Value(1000)"
set ylabel "Execution time in sec"

plot "seq.dat" using 2 title 'Sequential'  with linespoints, \
	 "FINAL_100_calc.dat" using 2 title 'OpenCl with workgroup size 25' with linespoints
