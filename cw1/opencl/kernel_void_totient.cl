/**
  *
  *@author Peter Kovacs
  *@date   05/03/2014
  *@course Distributed and Parallel Technologies
  */
//enable atomic operators on long
#pragma OPENCL EXTENSION cl_khr_int64_base_atomics : enable
__kernel void compute_sum_of_totient(__global long* result,unsigned int lower)
{

   //ids of thread
   size_t gx = get_global_id(0)+lower;
   size_t gy = get_global_id(1)+1; 

   //only half of the matrix has to calculate
   if(gy<gx){
      long t;
      //relative prime function
      while (gy != 0) {
        t = gx % gy;
        gx = gy;
        gy = t;
      }
      if(gx==1){
        //ids are relative primeatomic increment counter field
        atom_inc(&result[0]);          
      }
      
   }
  

}
