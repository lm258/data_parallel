#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <CL/cl.h>
#include "simple.h"
//#include "./euler/euler.h"

#define DATA_SIZE 10240000
#define MAX_KERNAL_SOURCE 1000
//extern long sumTotient(long lower, long upper);
char KernelSource[MAX_KERNAL_SOURCE] = {0};


void readKernelSource( const char * fileName ) {
  FILE * fp = fopen( fileName, "r" );
  int i = 0;
  while( !feof(fp) && i < MAX_KERNAL_SOURCE)
  {
    KernelSource[i] = (char)fgetc(fp);
    i++;
  }
  KernelSource[i-1] = '\0';
  fclose( fp );
}


struct timespec start, stop;

void printTimeElapsed( char *text)
{
  double elapsed = (stop.tv_sec -start.tv_sec)*1000.0
                  + (double)(stop.tv_nsec -start.tv_nsec)/1000000.0;
  printf( "%s: %f msec\n", text, elapsed);
}

int main (int argc, char * argv[])
{
  cl_int err;
  cl_kernel kernel;
  //create twodimensional ids
  size_t global[1];
  size_t local[1];  
  
  //read kernel source
  readKernelSource("kernel_void_totient.cl");

  unsigned int lower;
  long upper;
  //default w_size
  unsigned int w_size=4;

  if( argc !=4) {
	  printf("not enough arguments\n");
    return 1;
    
  } 
  else {
	  lower = atoi(argv[1]);
	  upper = atol(argv[2]);
    w_size = atoi(argv[3]);
    if(upper<= lower){
      printf("Upper must be greater than lower");
      return 1;
    }
    // check if range is divisible by work group size
    if((upper-lower+1)%w_size!= 0){
      printf("Global must be divisible by work group size. Global is %ld\n",(upper-lower+1));
      return 1;

  }
	  
  }

  // range of values
  int diff = upper-lower+1;

  //counter for the result
  long *result = NULL;  
  result = (long *) calloc (1,sizeof (long));
 
  //set local dimension values
  local[1] = w_size;
  local[0] = w_size;  
  
  //set global dimension values
  global[0] = diff;
  global[1] = upper;

  printf( "work group size: %d\n", (int)local[0]);
  printf( "global size: %d\n", (int)global[0]);
  

  //init GOU
  err = initGPU();  
  if( err == CL_SUCCESS) {
    kernel = setupKernel( KernelSource, "compute_sum_of_totient", 2,
                                                     LongArr,1,result,                                                
                                                     IntConst, lower);
                                                     
    clock_gettime( CLOCK_PROCESS_CPUTIME_ID, &start);  
    runKernel( kernel, 2, global, local);
  
    /*
    printf("Max workgroup size: %d\n",CL_DEVICE_MAX_WORK_GROUP_SIZE);
    printf("Max compute units: %d\n",CL_DEVICE_MAX_COMPUTE_UNITS);
    printf("Max work item dimensions: %d\n",CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS);
    printf("Max work item sizes: %d\n",CL_DEVICE_MAX_WORK_ITEM_SIZES);
    
    */

    unsigned long sum = 0;
    unsigned int i;
    for (i = 0; i < 1; i++) {
        sum += (long)result[i];
    }
    //optimization -- if lower value is 1 - add 1
    if(lower==1) sum+=1;
  
    
    printf ("Sum of Totients  between %d..%ld is %ld\n",lower, upper,sum);

    clock_gettime( CLOCK_PROCESS_CPUTIME_ID, &stop);
    printKernelTime();

    err = clReleaseKernel (kernel);
    err = freeDevice();

    return EXIT_SUCCESS;
    
  }


  return 0;
}
