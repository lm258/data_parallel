#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <omp.h>
#include "ann.h"

/*Create some little arrays to store our neural network in*/
float weights[GENOME_LENGTH] = {0};
float input[NETWORK_HEIGHT]  = {0};
float output[NETWORK_HEIGHT] = {0};

int main() {

    int i;

    /*Set example input data*/
    input[0] = 1.0f;
    input[1] = 2.0f;
    /*input[3] = 1.0f;
    input[2] = 2.0f;
    input[4] = 6.0f;
    input[5] = 1.0f;
    input[6] = 3.0f;
    input[7] = 5.0f;*/

    /*Set the sample weights for our input*/
    for(i=0; i<GENOME_LENGTH; i++){
        weights[i] = 1.0f;
    }

    struct timespec tstart={0,0}, tend={0,0};
    clock_gettime(CLOCK_MONOTONIC, &tstart);
    ann_calculate( weights, input, output);
    clock_gettime(CLOCK_MONOTONIC, &tend);
    printf("some_long_computation took about %.5f seconds\n",
               ((double)tend.tv_sec + 1.0e-9*tend.tv_nsec) - 
               ((double)tstart.tv_sec + 1.0e-9*tstart.tv_nsec));
    /*Print outputs*/
    for(i=0;i<NETWORK_HEIGHT; i++) {
        printf( "output: %f\n", output[i] );
    }
}