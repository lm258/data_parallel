#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <omp.h>

#ifndef _ANN_H_
#define _ANN_H_

/*Set to 1 for debug mode (prints calculations/ disabled activation for simplicity)*/
#define DEBUG 0

//#define PARALLEL

/*Network height - IE number of input neurons*/
#define NETWORK_HEIGHT 2 //Y
#define NETWORK_DEPTH NETWORK_HEIGHT //Z

/*This is the width X of the network*/
#define NETWORK_WIDTH 1300000

/*Genome length is the size of the neural network*/
#define GENOME_LENGTH NETWORK_WIDTH*NETWORK_DEPTH*NETWORK_HEIGHT

/**
 * Translates 3d coord into a 1d array
*/
uint8_t as3D(uint8_t x, uint8_t y, uint8_t z);

/**
 * This function will initalize the
 * neural network with random values.
 * float * network - The weights of the network
*/
void ann_init_network(float * network);

/**
 * This function is used to
 * calculate the ouput of the neural
 * network.
 * float * network - The weights of the network
 * float * input   - The input
 * float * output  - The output
*/
void ann_calculate( float * network, float * input, float * output );


/**
 * Print the artificial neural network
 * float * network - The weights of the network
*/
void ann_print( float * network );

#endif