#include "ann.h"


/**
 * Translates 3d coord into a 1d array coord
*/
uint8_t as3D(uint8_t x, uint8_t y, uint8_t z){
    return x + NETWORK_WIDTH * (y + NETWORK_DEPTH * z);
}

/**
 * This function will initalize the
 * neural network.
 * float * network - The weights of the network
*/
void ann_init_network(float * network) {
    uint8_t i;
    for(i=0; i<GENOME_LENGTH; i++) {
        network[i] = (float)rand() / (float)RAND_MAX;
    }
}

/**
 * This function is used to
 * calculate the ouput of the neural
 * network.
 * float * network - The weights of the network
 * float * input   - The input
 * float * output  - The output
*/
void ann_calculate( float * network, float * inputRaw, float * output ) {
    int x,y,z;
    float * input = (float *)calloc( NETWORK_HEIGHT, sizeof(float) );
    memcpy( input, inputRaw, NETWORK_HEIGHT * sizeof(float) );

    omp_set_num_threads( NETWORK_HEIGHT );

    #pragma omp parallel private(x,y,z) 
    {
        for(x=0; x<NETWORK_WIDTH; x++) {
            memset( output, 0, NETWORK_HEIGHT * sizeof(float) );
            #pragma omp for schedule(static)
            y = omp_get_thread_num();
            for(y=0;y<NETWORK_HEIGHT; y++) {
                for(z=0; z<NETWORK_DEPTH; z++) {
                    if(DEBUG==1){
                        printf("outN[%d](%f) += inN[%d](%f) * weight(%f)\n", z, output[z], y, input[y], network[ as3D(x,y,z) ]);
                    }
                    output[z] += input[y] * network[ as3D(x,y,z) ];
                }
            }

            #pragma omp barrier

            /*Get thread id and set output at that position to zero, sync threads*/
            y = omp_get_thread_num();
            output[y] = 0;

            /*Compute our output*/
            for(z=0; z<NETWORK_DEPTH; z++) {
                float inc = input[y] * network[ as3D(x,y,z) ];
                #pragma omp critical
                {
                    output[z] += inc;
                }
            }
            #pragma omp barrier

            output[y] = (output[y] / (1 + abs(output[y])));
            #pragma omp barrier

            if( x < NETWORK_WIDTH-1 ){
                input[y] = output[y];
            }
            #pragma omp barrier

        }
    }
    free(input);
}

/**
 * Print the artificial neural network
 * float * network - The weights of the network
*/
void ann_print( float * network ) {
    uint8_t x,y,z;
    for(x=0; x<NETWORK_WIDTH; x++) {
        for(y=0;y<NETWORK_HEIGHT; y++) {
            for(z=0; z<NETWORK_DEPTH; z++) {
                printf( "%f\n", network[ as3D(x,y,z) ] );
            }
        }
    }
}