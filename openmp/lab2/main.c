int main()  
{   
    int i;  double  x,  pi, sum =0.0;       
    step = 1.0/(double)num_steps;  
    omp_set_num_threads(10);    
    #pragma omp parallel
    {   
        double x;
        #pragma omp for static(1)
        for(i=0;i<num_steps;i++) {
            x = (i+0.5) * step;   
            x = 4.0/(1.0+x*x);  
            #pragma omp atomic  
            {
                sum +=  x;
            }
        }   
    }   
    pi = sum * step;
}