--sequential profiling

--ghc -cpp -O2 -rtsopts -prof -auto-all -o totient_hs totient.hs
--hp2ps totient_hs.hp
--ps2pdf totient_hs.ps

-- Compile: ghc -O2 -rtsopts -threaded -o totient_thr totient.hs
-- Seq Run: ./totient_thr 1 100 +RTS -N1    
-- Par Run: ./totient_thr 1 100 +RTS -N6 
-- use  +RTS -N6 -sstderr for more detailed output, e.g. number of sparks generated
-- 
-- For parallel profiling using threadscope:
-- Compile: ghc -O2 -rtsopts -threaded -eventlog -o totient_thr_l totient.hs
-- Run:    ./totient_thr_l 1 100 +RTS -N6 -ls
-- Visualise:  /home/pt114/.cabal/bin/threadscope totient_thr_l.eventlog
--
------



---------------------------------------------------------------------------
-- Sequential Euler Totient Function 
---------------------------------------------------------------------------
-- This program calculates the sum of the totients between a lower and an 
-- upper limit, using arbitrary precision integers.
-- Phil Trinder, 26/6/03
-- Based on earlier work by Nathan Charles, Hans-Wolfgang Loidl and 
-- Colin Runciman
---------------------------------------------------------------------------

module Main(main) where

import System.Environment
import System.IO
import Control.Parallel
import Control.Parallel.Strategies
import Control.DeepSeq

---------------------------------------------------------------------------
-- Main Function, sumTotient
---------------------------------------------------------------------------
-- The main function, sumTotient  
-- 1. Generates a list of integers between lower and upper
-- 2. Applies Euler's phi function to every element of the list
-- 3. Returns the sum of the results

sumTotient :: Int -> Int -> Int
sumTotient lower upper = sum (map euler [lower, lower+1 .. upper] `using` parListChunk 50 rdeepseq)

---------------------------------------------------------------------------
-- euler
---------------------------------------------------------------------------
-- The euler n function
-- 1. Generates a list [1,2,3, ... n-1,n]
-- 2. Select only those elements of the list that are relative prime to n
-- 3. Returns a count of the number of relatively prime elements

euler ::  Int -> Int
euler n = sum (map (relprime n) [1 .. n-1])

-- `using` parListChunk 1000 rdeepseq)




---------------------------------------------------------------
-- relprime
---------------------------------------------------------------------------
-- The relprime function returns true if it's arguments are relatively 
-- prime, i.e. the highest common factor is 1.

relprime :: Int -> Int -> Int
relprime x y = if (hcf x y == 1) then 1 else 0

---------------------------------------------------------------------------
-- hcf 
---------------------------------------------------------------------------
-- The hcf function returns the highest common factor of 2 integers

hcf :: Int -> Int -> Int
hcf x 0 = x
hcf x y = hcf y (rem x y)

---------------------------------------------------------------------------
-- mkList
---------------------------------------------------------------------------
-- enumerate the interval in reverse order
mkList :: Int -> Int -> [Int]
mkList lower upper = reverse (enumFromTo lower upper)

---------------------------------------------------------------------------
-- Interface Section
---------------------------------------------------------------------------

main = do args <- getArgs
          let 
            lower = read (args!!0) :: Int -- lower limit of the interval
            upper = read (args!!1) :: Int -- upper limit of the interval
          hPutStrLn stderr ("Sum of Totients between [" ++ 
                      (show lower) ++ ".." ++ (show upper) ++ "] is " ++ 
                       show (sumTotient lower upper))