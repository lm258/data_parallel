#!/bin/bash
export TIMEFORMAT='%E';

rm -f parHaskellDSA1.dat;
for i in `seq 1 1 8`; do
    utime=`time ( ./par_tot 1 15000 +RTS -N$i) 2>&1 1>/dev/null`;
    echo "$i, $utime";
    echo "$i, $utime" >> parHaskellDSA1.dat;
done
