#!/bin/bash
export TIMEFORMAT='%E';

rm -f sequential.dat;
for i in `seq 1000 1000 30000`; do
    utime=`time ( ./seq_tot 1 $i ) 2>&1 1>/dev/null`;
    echo "$i, $utime";
    echo "$i, $utime" >> seqC.dat;
done
