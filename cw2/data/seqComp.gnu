# set terminal png transparent nocrop enhanced size 450,320 font "arial,8" 
# set output 'simple.8.png'
#set key bmargin left horizontal Right noreverse enhanced autotitle box lt black linewidth 1.000 dashtype solid
set samples 800, 800
set title "Execution Time of Sequential C and Haskell" 
set title  font ",20" norotate
set datafile separator ","
set term png
set output 'comp.png'
set style data points
set xlabel "Upper Value(1000)"
set ylabel "Execution time in sec"

plot "seqC.dat" using 2 title 'Sequential C'  with linespoints, \
	 "seqHaskell.dat" using 2 title 'Sequential Haskell' with linespoints
