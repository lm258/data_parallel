#!/bin/bash
#Arguments are $1=Name of the .dat file
#              $2=Make command ie run or run-cluster
#              $3=Range to start at eg 15000 or 1000
#              $4=End range
#              $5=Number of threads
export TIMEFORMAT='%E';
for i in `seq $3 1000 $4`; do
    times=(0 0 0)
    for j in 0 1 2; do
        times[$j]=`time ( ./eulers_sum -mt $5 1 $i ) 2>&1 1>/dev/null`;
    done
    utime=`echo "( ${times[0]}+${times[1]}+${times[2]} )/3" | bc -l`
    echo "$i, $utime";
    echo "$i, $utime" >> $1.dat;
done