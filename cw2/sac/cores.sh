#!/bin/bash
# Arguments:
#  $1  - Function to run eg run or run-cluster(MPI only)
#  $2  - Max number of cores
#  $3  - End of range
#  $4  - Name of the output file
export TIMEFORMAT='%E';
for i in `seq 1 1 $2`; do
    times=(0 0 0)
    for j in 0 1 2; do
        times[$j]=`time ( ./eulers_sum -mt $i 1 $3 ) 2>&1 1>/dev/null`;
    done
    utime=`echo "(${times[0]}+${times[1]}+${times[2]})/3" | bc -l`;
    echo "$i, $utime";
    echo "$i, $utime" >> $4.dat;
done
